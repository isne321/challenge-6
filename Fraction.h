#ifndef FRACTION_H
#define FRACTION_H

class fraction {

	public:
		//Constructor
		fraction();
		fraction(int new_num);
		fraction(int new_num, int new_denom);
		//Accessor
		int get_num();
		int get_denom();
		//Mutator
		void set_num(int new_num);
		void set_denom(int new_denom);
		//Input / Output fraction
		void input();
		void display();
		//Irreducible fraction
		void min();
		//Compare two fractions
		friend bool operator ==(fraction f1, fraction f2);
		friend bool operator !=(fraction f1, fraction f2);
		friend bool operator <(fraction f1, fraction f2);
		friend bool operator <=(fraction f1, fraction f2);
		friend bool operator >(fraction f1, fraction f2);
		friend bool operator >=(fraction f1, fraction f2);
		//+ - * /
		friend fraction operator +(fraction f1, fraction f2);
		friend fraction operator -(fraction f1, fraction f2);
		friend fraction operator *(fraction f1, fraction f2);
		friend fraction operator /(fraction f1, fraction f2);
	private:
		int num;
		int denom;
};
#endif //FRACTION_H