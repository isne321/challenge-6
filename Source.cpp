#include<iostream>
#include"Fraction.h"

using namespace std;

int main() {
	fraction f1(35,70);
	fraction f2;

	f1.min();
	f2.set_num(2);
	f2.input();

	f1.display();
	cout << endl;

	f2.min();

	f2.display();
	cout << endl;

	f1.display();
	if (f1 != f2) {
		cout << "these two fraction are NOT equal";
	}
	return 0;
}