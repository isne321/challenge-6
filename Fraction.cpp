#include <iostream>
#include <cstdlib>
#include "fraction.h"

using namespace std;

fraction::fraction() {
	num = 1;
	denom = 1;
}
fraction::fraction(int new_num) {
	num = new_num;
	denom = 1;
}
fraction::fraction(int new_num, int new_denom) {
	num = new_num;
	denom = new_denom;
}
int fraction::get_num() {
	return num;
}
int fraction::get_denom() {
	return denom;
}
void fraction::set_num(int new_num) {
	num = new_num;
}
void fraction::set_denom(int new_denom) {
	denom = new_denom;
}
void fraction::input() { //Input Fraction
	cout << "Enter numerator : ";
	cin >> num;
	cout << "Enter Denominator : ";
	cin >> denom;
}
void fraction::display() { //Display Fraction
	cout << num << '/' << denom;
}
void fraction::min() {  //Make fraction irreducible
	
	while ((num % 2 == 0 && denom % 2 == 0) || (num % 3 == 0 && denom % 3 == 0) || (num % 5 == 0 && denom % 5 == 0) || (num % 7 == 0 && denom % 7 == 0) ||
		(num % 11 == 0 && denom % 11 == 0) || (num % 2 == 13 && denom % 13 == 0) || (num % 17 == 0 && denom % 17 == 0)) {
		//Divided by using Prime Number
		if (num % 2 == 0 && denom % 2 == 0) {
			num = num / 2;
			denom = denom / 2;
		}
		if (num % 3 == 0 && denom % 3 == 0) {
			num = num / 3;
			denom = denom / 3;
		}
		if (num % 5 == 0 && denom % 5 == 0) {
			num = num / 5;
			denom = denom / 5;
		}
		if (num % 7 == 0 && denom % 7 == 0) {
			num = num / 7;
			denom = denom / 7;
		}
		if (num % 11 == 0 && denom % 11 == 0) {
			num = num / 11;
			denom = denom / 11;
		}
		if (num % 13 == 0 && denom % 13 == 0) {
			num = num / 13;
			denom = denom / 13;
		}
		if (num % 17 == 0 && denom % 17 == 0) {
			num = num / 17;
			denom = denom / 17;
		}
	}
}

bool operator ==(fraction f1, fraction f2) { //Compare fraction if they're eqaul
	if ((f1.num == f2.num) && (f1.denom == f2.denom)) {
		return true;
	}
	else { 
		return false;
	}
}

bool operator !=(fraction f1, fraction f2) { //Compare fraction if they're NOT eqaul
	if ((f1.num != f2.num) || (f1.denom != f2.denom)) {
		return true;
	}
	else {
		return false;
	}	
}
bool operator <(fraction f1, fraction f2) { //Compare fraction if they're less than another
	if ((f1.denom == f2.denom) && (f1.num < f2.num)){
		return true;
	}
	else {
		return false;
	}
}

bool operator <=(fraction f1, fraction f2) { //Compare fraction if they're less than or eqaul another
	if ((f1.denom == f2.denom) && ((f1.num < f2.num) || (f1.num==f2.num))) {
		return true;
	}
	else {
		return false;
	}
}

bool operator >(fraction f1, fraction f2) { //Compare fraction if they're greater than another
	if ((f1.denom == f2.denom) && (f1.num > f2.num)) {
		return true;
	}
	else {
		return false;
	}
}

bool operator >=(fraction f1, fraction f2) { //Compare fraction if they're greater than or eqaul another
	if ((f1.denom == f2.denom) && ((f1.num > f2.num) || (f1.num == f2.num))) {
		return true;
	}
	else {
		return false;
	}
}

fraction operator +(fraction f1, fraction f2) { //Combines two fraction
	
	fraction result;
	if (f1.denom == f2.denom) { //Check if denom is the same number
		result.denom = f1.denom;
		result.num = f1.num + f2.num;
	}
	else {
		result.num = (f1.num * f2.denom) + (f1.denom * f2.num);
		result.denom = f1.denom * f2.denom;
		result.min(); //Make fraction irreducible
	}
	return result;
}
fraction operator -(fraction f1, fraction f2) { //Substracts two fraction

	fraction result;
	if (f1.denom == f2.denom) { //Check if denom is the same number
		result.denom = f1.denom;
		result.num = f1.num - f2.num;
	}
	else {
		result.num = (f1.num * f2.denom) - (f1.denom * f2.num);
		result.denom = f1.denom * f2.denom;
		result.min(); //Make fraction irreducible
	}
	return result;
}
fraction operator *(fraction f1, fraction f2) { //Times two fraction
	fraction result;
	result.num = f1.num * f2.num;
	result.denom = f1.denom * f2.denom;
	result.min(); //Make fraction irreducible
	return result; 
}
fraction operator /(fraction f1, fraction f2) { //Devided two fraction
	fraction result;
	result.num = f1.num * f2.denom;
	result.denom = f1.denom * f2.num;
	result.min(); //Make fraction irreducible
	return result;
}